from django.apps import AppConfig


class KodebanConfig(AppConfig):
    name = 'kodeban'
