from django.urls import path

from . import views

urlpatterns = [
    path('task', views.TaskList.as_view(), name='task-list'),
    path('task/<int:pk>', views.TaskDetail.as_view(), name="task-detail"),
    path(
        'task/column/<int:column_id>',
        views.TaskColumnView.as_view(),
        name='task-list-column'
    ),

    path('column', views.ColumnList.as_view(), name='column-list'),
    path(
        'column/<int:pk>',
        views.ColumnDetail.as_view(),
        name='column-detail'
    ),

    path('subtask', views.SubtaskList.as_view(), name='subtask-list'),
    path(
        'subtask/<int:pk>',
        views.SubtaskDetail.as_view(),
        name='subtask-detail'
    ),
    path(
        'subtask/task/<int:task_id>',
        views.SubtaskList.as_view(),
        name='subtask-task-list'
    ),
    path('task/priority', views.PriorityView.as_view(), name='priority'),
    path('member', views.MemberList.as_view(), name='member-list'),
    path(
        'member/<int:pk>',
        views.MemberDetail.as_view(),
        name='member-detail'
    ),
    path(
        'member/task/<int:task_id>/in',
        views.in_task,
        name='member-in-task'
    ),
    path(
        'member/task/<int:task_id>/notIn',
        views.not_in_task,
        name='member-not-in-task'
    ),
]
