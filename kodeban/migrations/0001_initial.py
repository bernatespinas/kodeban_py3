# Generated by Django 3.1.7 on 2021-02-19 12:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kolumn',
            fields=[
                ('id', models.IntegerField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.IntegerField(auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('details', models.CharField(max_length=200)),
                ('done', models.BooleanField(default=False)),
                ('column_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kodeban.kolumn')),
            ],
        ),
        migrations.CreateModel(
            name='Subtask',
            fields=[
                ('id', models.IntegerField(auto_created=True, primary_key=True, serialize=False)),
                ('details', models.CharField(max_length=100)),
                ('checked', models.BooleanField(default=False)),
                ('task_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kodeban.task')),
            ],
        ),
    ]
