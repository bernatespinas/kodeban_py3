# Generated by Django 3.1.7 on 2021-02-19 12:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kodeban', '0003_auto_20210219_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kolumn',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='subtask',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='task',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
