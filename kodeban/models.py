from django.db import models


class Kolumn(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)

    class Meta:
        db_table = 'kolumn'


class Priority(models.TextChoices):
    LOW = 'low', 'Low'
    MEDIUM = 'medium', 'Medium'
    HIGH = 'high', 'High'
    URGENT = 'urgent', 'Urgent'


class Task(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    details = models.CharField(max_length=200, default='')
    # Appends '_id' at the end, would be `column_id_id`
    column_id = models.ForeignKey(
        Kolumn,
        db_column='column_id',
        on_delete=models.CASCADE
    )
    priority = models.CharField(
        max_length=6,
        choices=Priority.choices,
        default=Priority.LOW
    )
    done = models.BooleanField(default=False)

    class Meta:
        db_table = 'task'


class Subtask(models.Model):
    id = models.AutoField(primary_key=True)
    details = models.CharField(max_length=100)
    # Appends '_id' at the end, 'task_id' didn't work
    task_id = models.ForeignKey(
        Task,
        db_column='task_id',
        on_delete=models.CASCADE
    )
    checked = models.BooleanField(default=False)

    class Meta:
        db_table = 'subtask'


class Member(models.Model):
    id = models.AutoField(primary_key=True)
    full_name = models.CharField(max_length=50)
    email = models.EmailField()
    picture_url = models.CharField(max_length=200)

    tasks = models.ManyToManyField(Task, db_table='task_member')

    class Meta:
        db_table = 'member'
