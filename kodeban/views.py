# from django.shortcuts import render
from .models import Task, Kolumn, Subtask, Member

from django.db import connection

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
from rest_framework.decorators import api_view


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class TaskDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskList(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskColumnView(APIView):
    def get(self, request, column_id):
        # tasks = get_list_or_404(Task, column_id=column_id)
        # Why does '-priority' work? Is it because of the order in which
        # I've defined the enum variants?
        tasks = Task.objects.filter(column_id=column_id).order_by('-priority')
        json = TaskSerializer(tasks, many=True).data

        return Response(json)


class ColumnSerializer(ModelSerializer):
    class Meta:
        model = Kolumn
        fields = '__all__'


class ColumnList(generics.ListAPIView):
    queryset = Kolumn.objects.all()
    serializer_class = ColumnSerializer


class ColumnDetail(generics.RetrieveUpdateAPIView):
    queryset = Kolumn.objects.all()
    serializer_class = ColumnSerializer


class SubtaskSerializer(ModelSerializer):
    class Meta:
        model = Subtask
        fields = '__all__'


class SubtaskDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Subtask.objects.all()
    serializer_class = SubtaskSerializer


class SubtaskList(generics.ListCreateAPIView):
    queryset = Subtask.objects.all()
    serializer_class = SubtaskSerializer

    def get(self, request, task_id: int):
        subtasks = Subtask.objects.filter(task_id=task_id)
        json = SubtaskSerializer(subtasks, many=True).data

        return Response(json)


class PriorityView(APIView):
    def get(self, request):
        row = []

        with connection.cursor() as cursor:
            cursor.execute("SELECT UNNEST(ENUM_RANGE(NULL::priority_enum))")
            row = cursor.fetchall()

        # row is a list of tuples, like
        # `[('low',), ('medium',), ('high',), ('urgent',)]`,
        # so I have to unpack it.
        response = []
        for x in row:
            response.append(*x)
        return Response(response)


class MemberSerializer(ModelSerializer):
    class Meta:
        model = Member
        fields = '__all__'


class MemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer


class MemberList(generics.ListCreateAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer


@api_view()
def in_task(request, task_id: int):
    members = Member.objects.filter(tasks__id=task_id)
    json = MemberSerializer(members, many=True).data

    return Response(
        json
    )


@api_view()
def not_in_task(request, task_id: int):
    members = Member.objects.exclude(tasks__id=task_id)
    json = MemberSerializer(members, many=True).data

    return Response(json)
